# Badbots

From their repo

> The Ultimate Nginx Bad Bot, User-Agent, Spam Referrer Blocker, Adware, Malware and Ransomware Blocker, Clickjacking Blocker, Click Re-Directing Blocker, SEO Companies and Bad IP Blocker with Anti DDOS System, Nginx Rate Limiting and Wordpress Theme Detector Blocking. Stop and Block all kinds of bad internet traffic even Fake Googlebots from ever reaching your web sites.

---

## How to update this repo

**Please note:** You will see inline code edits with the word GLOBAL. This repo is upstream of the private bd-base-i8 repo which is considered LOCAL in comments.

* Git clone this repo to your local
  * `git clone https://gitlab.com/born-digital-public/bad_bot.git`

* Run the `./update.sh` script which will:
  * create a new git feature branch e.g. (_mnt-update-<month>-<year> or mnt-update-01-2024_)
  * wget only two files (_codebase/robots.txt & http.d/globalblacklist.conf_) from the upstream Github.com [nginx-ultimate-bad-bot-blocker](https://github.com/mitchellkrogza/nginx-ultimate-bad-bot-blocker) repo.
    * This will overwrite the two files but for `codebase/robots.txt`, the script will then append the BD custom changes from `codebase/bd_robots_custom.txt` **back** to the end of the file.

* While there should not be any merge conflicts, double check and add any additional changes stemming from recent project issues or attacks on any remaining files typically `rootfs/etc/nginx/bad_bot/bots.d/blacklist-ips.conf` or anything else from the remaining list below.

* Once all edits are done, check in and push the feature branch. 
* Merge the feature branch into the `main` branch when ready. 
* The private bd-base-i8 repo will pull in the new changes from this upstream upon the next image build.
* All downstream projects will be updated during the schedule release deploys.

---

## Files to consider updating monthly manually due to attacks or blocking monthly of other ISLE projects

In some cases, you may need to update files due to crawlers, scanners, rogue IP addresses etc. having attacked certain BD projects and all projects need to block them not just a few.

Review the GLOBAL files as needed:

### bots.d

* [rootfs/etc/nginx/bad_bot/bots.d/bad-referrer-words.conf](https://gitlab.com/born-digital-us/bd-base-i8/bd-base-i8/-/blob/staging/rootfs/etc/nginx/bad_bot/bots.d/bad-referrer-words.conf?ref_type=heads)
  * Blocks by strings or paths as needed eg. `/wp-login` is often found. Use of this file should be expanded, currently in `codebase/robots.txt`.

* [rootfs/etc/nginx/bad_bot/bots.d/blacklist-ips.conf](https://gitlab.com/born-digital-us/bd-base-i8/bd-base-i8/-/blob/staging/rootfs/etc/nginx/bad_bot/bots.d/blacklist-ips.conf?ref_type=heads)
  * Blocks by IP addresses

* [rootfs/etc/nginx/bad_bot/bots.d/blacklist-user-agents.conf](https://gitlab.com/born-digital-us/bd-base-i8/bd-base-i8/-/blob/staging/rootfs/etc/nginx/bad_bot/bots.d/blacklist-user-agents.conf?ref_type=heads)
  * Blocks by user agent / browser headers etc - used often to block ai, seo and spam bots, crawlers etc.

* [rootfs/etc/nginx/bad_bot/bots.d/custom-bad-referrers.conf](https://gitlab.com/born-digital-us/bd-base-i8/bd-base-i8/-/blob/staging/rootfs/etc/nginx/bad_bot/bots.d/custom-bad-referrers.conf?ref_type=heads)
  * Haven't had to use this much but we could block by services or other sites that have embedded links e.g. Facebook, Amazon etc

### robots.txt

* `codebase/robots.txt`
  * located in ISLE: `codebase/robots.txt`
  * located in bad-bots repo [robots.txt/robots.txt](https://github.com/mitchellkrogza/nginx-ultimate-bad-bot-blocker/blob/master/robots.txt/robots.txt)
  * Contains all updates from the maintainer who works in security field and keeps the blocklists up to date.
  * Merged with the standard Drupal robots.txt
  * Add User-agents if need be otherwise stick to updating the Crawler timeout values etc.

---

### Badbots structure as used in ISLE 2

The list below is all files as used in ISLE 2 however not all files need monthly updates if ever. Additionally files that have the `.toml` and `.tmpl` suffixes were originally used by badbots and then converted to [confd](https://github.com/kelseyhightower/confd) syntax for use of dynamic variables e.g. IP addresses for badbots to functions properly in ISLE

```bash
bd-base-i8-staging/rootfs/etc/nginx/bad_bot
├── bots.d
│   ├── bad-referrer-words.conf
│   ├── blacklist-ips.conf
│   ├── blacklist-user-agents.conf
│   ├── blockbots.conf
│   ├── custom-bad-referrers.conf
│   ├── ddos.conf
│   ├── whitelist-domains.conf
│   └── whitelist-ips.conf
├── confd
│   ├── conf.d
│   │   ├── whitelist-domains.conf.toml
│   │   └── whitelist-ips.conf.toml
│   └── templates
│       ├── nginx.conf.tmpl
│       ├── whitelist-domains.conf.tmpl
│       └── whitelist-ips.conf.tmpl
└── http.d
    ├── botblocker-nginx-settings.conf
    ├── default.conf
    └── globalblacklist.conf

6 directories, 16 files
```

---

### Resources for bad bots Blocking

Please feel free to add to this list for reference sites of bad bots to block.

* [nginx-ultimate-bad-bot-blocker](https://github.com/mitchellkrogza/nginx-ultimate-bad-bot-blocker)
  * Has steps to test in README
* [Drupal robots.txt](https://www.drupal.org/node/22265) prior to merge for reference and occassional diff if needed.
* [List](https://neil-clarke.com/block-the-bots-that-feed-ai-models-by-scraping-your-website/) of AI crawlers to block and explinations why.
* [List](https://securewp.co/bots-you-should-block-to-protect-your-content/) of bots to block for bandwidth and security.
