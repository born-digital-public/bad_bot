#!/bin/bash

echo "Refresh from main branch"
git checkout main
git pull origin main

echo "Create new git branch from main for monthly update"
git checkout -b $(date +mnt-update-%m-%Y)

# Update conf.d/globalblacklist.conf - straight overwrite no further customization
echo "update conf.d/globalblacklist.conf from upstream source nginx-ultimate-bad-bot-blocker git repo"
wget -O http.d/globalblacklist.conf https://raw.githubusercontent.com/mitchellkrogza/nginx-ultimate-bad-bot-blocker/master/conf.d/globalblacklist.conf

# Update robots.txt/robots.txt
echo "update robots.txt from upstream source nginx-ultimate-bad-bot-blocker git repo"
wget -O codebase/robots.txt https://raw.githubusercontent.com/mitchellkrogza/nginx-ultimate-bad-bot-blocker/master/robots.txt/robots.txt
echo "add back BD robots.txt customizations to end of file"

# Check if the source file exists
source_file="codebase/bd_robots_custom.txt"
destination_file="codebase/robots.txt"

if [ -f "$source_file" ]; then
    # Append the contents of the source file to the destination file
    cat "$source_file" >> "$destination_file"
    echo "Contents appended successfully."
else
    echo "Source file '$source_file' not found."
fi

echo "Script complete. Please update remaining files manually from other projects and check for merge conflicts as needed"
exit;